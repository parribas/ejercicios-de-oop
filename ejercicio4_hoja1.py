#Crea una clase Fraccion con métodos para sumar, restar, multiplicar y dividir fracciones.

class Fraccion:
    def __init__(self, num=0.0, den= 1.0):
        pass

    def sumar(izq, dcha): # es lo mismo que suma?
        return Fraccion(izq.numerador * dcha.denominador + dcha.numerador * izq.denominador)
    
    def suma(self, otro): # o ponemos sumar?, esta es un sumamecon 
        self.numerador = self.numerador * otro.denominador + blabla
        self.denominador = self.denominador * otro.denominador

        return self
    
    #def resta(self, ??): # o ponemos restar?
    #    pass
    
    def __add__(self, other):
        """
        Suma dos fracciones
        """
        return algo

unmedio = Fraccion(1,2)
uno =Fraccion(1)
cero = Fraccion()

#misuma = uno + unmedio
#misuma = uno.suma(unmedio)
misuma = Fraccion.sumar(uno, unmedio) # esta es la mejor forma de redactarlo

class TestFraccion(unittest.Testcase):

    def test_suma01(self):

        f1 = Fraccion(1,2)
        f2 = Fraccion(1,2)

        suma = f1.suma(f2)

        self.assertEqual(suma.numerador, 1)
        self.assertEqual(suma.denominador, 1)
    
    def test_suma02(self):

        f1 = Fraccion(1,2)
        f2 = Fraccion(-1,2)

        suma = f1.suma(f2)

        self.assertEqual(suma.numerador, 0)
        self.assertEqual(suma.denominador, 1)

    def test_sumar(self):

        f1 = Fraccion(1,2)
        f2 = Fraccion(1,2)

        suma = Fraccion.sumar(f1,f2)

        self.assertEqual(suma.numerador, 1)
        self.assertEqual(suma.denominador, 1)
